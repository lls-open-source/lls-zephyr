/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"


#define SIGNAL_PACKET_LENGTH	2U
#define SIGNAL_PACKET_ADDRESS	0x4C
#define SIGNAL_PACKET_COMMAND	0x1F

union packet_send {
	uint8_t buf[SIGNAL_PACKET_LENGTH+1]; // +1 for length byte itself
	struct {
		uint8_t len;
		uint8_t addr;
		uint8_t cmd;
	} __packed;
};

void print_packet(union packet_send *pkt)
{
	printk("Sending signal: len=%d -- addr=%02X -- cmd=%02X\n",
			pkt->len, pkt->addr, pkt->cmd);
}

struct glob global;

void main(void)
{
	const struct device *rf;
	union packet_send pkt;
	const struct device *usb;

	usb = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);

	if (usb_enable(NULL)) {
		return;
	}

	while (!device_is_ready(usb)) {
		k_sleep(K_MSEC(100));
	}

	rf = device_get_binding("CC1101");
	if (rf == NULL) {
		return;
	}

	pkt.len = SIGNAL_PACKET_LENGTH;
	pkt.addr = SIGNAL_PACKET_ADDRESS;
	pkt.cmd = SIGNAL_PACKET_COMMAND;


	k_sem_init(&global.trigger_sem, 0, 1);
	global.triggered = true;
	k_sem_give(&global.trigger_sem);

	if(gpio_setup()) {
		printf("Failed setting up GPIO devices.");
		return;
	}

	rf = device_get_binding("CC1101");
	if (rf == NULL) {
		return;
	}

	while(1){
		if(global.triggered == true){

			if(rf_send(rf, (union rf_packet *)&pkt) == 0) {
				print_packet(&pkt);
			}

			k_sleep(APP_WAIT_INTERVAL);

			/* send twice */
			if(rf_send(rf, (union rf_packet *)&pkt) == 0) {
				print_packet(&pkt);
				//rf_print_status(rf);
			}

			if(k_sem_take(&global.trigger_sem, APP_WAIT_INTERVAL) == 0) {
				global.triggered = false;
				k_sem_give(&global.trigger_sem);
			}

		}
		k_sleep(APP_WAIT_INTERVAL);
	}
}
