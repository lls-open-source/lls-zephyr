#ifndef APP_CC1101_H_
#define APP_CC1101_H_

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <drivers/rf.h>
#include <drivers/rf/cc1101.h>
#include <usb/usb_device.h>
#include <drivers/uart.h>
#include <stdio.h>

#define APP_WAIT_INTERVAL K_MSEC(100)

struct glob {
	struct k_sem trigger_sem;
	bool triggered;
};

extern struct glob global;


int gpio_setup(void);
void button_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void relay_triggered(const struct device *dev, struct gpio_callback *cb, uint32_t pins);


static inline int rssi_to_dbm(uint8_t rssi)
{
	int rssi_dbm;

	if (rssi >= 128){
		rssi_dbm = ((float)rssi - 256U) / 2;
	} else {
		rssi_dbm = (float)rssi / 2;
	}
	rssi_dbm = rssi_dbm - 74U;
	return rssi_dbm;
}

#endif // APP_CC1101_H_
