.. _sgp40_sht4x:

SGP40 and SHT4X: High accuracy digital I2C humidity sensor and multipixel gas sensor
####################################################################################

Description
***********

This sample application periodically measures the ambient temperature, humidity
and a raw gas sensor value from an SGP40 and SHT4X device.
The result is written to the console.

You can choose to use the on-chip T/RH compensation of the SGP40
by feeding the values measured by the SHT4X into it.
This is enabled in the Application by default, you can turn it off
by setting :code:`APP_USE_COMPENSATION=n`.

The SHT4X has the option to use a heater which makes sense for specific
environments/applications (refer to the datasheet for more information).
To make use of the heater have a look at the Kconfig options for this application.


References
**********

 - `SHT4X sensor <https://www.sensirion.com/en/environmental-sensors/humidity-sensors/humidity-sensor-sht4x/>`_
 - `SGP40 sensor <https://www.sensirion.com/en/environmental-sensors/gas-sensors/sgp40/>`_

Wiring
******

This sample uses the SHT4X and SGP40 sensor controlled using the I2C interface.
Connect Supply: **VDD**, **GND** and Interface: **SDA**, **SCL**.
The supply voltage can be in the 1.7V to 3.6V range.
Depending on the baseboard used, the **SDA** and **SCL** lines require Pull-Up
resistors.

Building and Running
********************

This project outputs sensor data to the console. It requires a SHT4X and a SGP40
sensor. It should work with any platform featuring a I2C peripheral
interface. This example has an device tree overlays for the :code:`blackpill_f411ce` and the :code:`seeeduino_xiao`
board.
You can configure the sampling rate and initial blackout period via :code:`-t menuconfig`.

.. code-block:: console

        west build -b blackpill_f411ce zephyr-lls2-app -t menuconfig
        west build -b blackpill_f411ce zephyr-lls2-app
        west flash



Sample Output
=============

.. code-block:: console

        picocom -b 115200 /dev/ttyUSB0

        Temp. [C]: 24.84 -- RH [%]: 57.76 -- Gas [a.u.]: 30115 -- VOC [a.u.]: 0
        Temp. [C]: 24.88 -- RH [%]: 57.72 -- Gas [a.u.]: 30110 -- VOC [a.u.]: 0
        Temp. [C]: 24.88 -- RH [%]: 57.70 -- Gas [a.u.]: 30111 -- VOC [a.u.]: 0
        Temp. [C]: 24.86 -- RH [%]: 57.66 -- Gas [a.u.]: 30106 -- VOC [a.u.]: 0

        <blackout period...>

        Temp. [C]: 24.85 -- RH [%]: 57.47 -- Gas [a.u.]: 30130 -- VOC [a.u.]: 0
        Temp. [C]: 24.86 -- RH [%]: 57.47 -- Gas [a.u.]: 30127 -- VOC [a.u.]: 1
        Temp. [C]: 24.86 -- RH [%]: 57.47 -- Gas [a.u.]: 30115 -- VOC [a.u.]: 1
        Temp. [C]: 24.87 -- RH [%]: 57.49 -- Gas [a.u.]: 30126 -- VOC [a.u.]: 2
        Temp. [C]: 24.84 -- RH [%]: 57.50 -- Gas [a.u.]: 30131 -- VOC [a.u.]: 3

        <later...>

        Temp. [C]: 24.87 -- RH [%]: 57.54 -- Gas [a.u.]: 30126 -- VOC [a.u.]: 92
        Temp. [C]: 24.85 -- RH [%]: 57.53 -- Gas [a.u.]: 30119 -- VOC [a.u.]: 93
        Temp. [C]: 24.88 -- RH [%]: 57.55 -- Gas [a.u.]: 30114 -- VOC [a.u.]: 93
        Temp. [C]: 24.87 -- RH [%]: 57.54 -- Gas [a.u.]: 30123 -- VOC [a.u.]: 94
        Temp. [C]: 24.89 -- RH [%]: 57.54 -- Gas [a.u.]: 30119 -- VOC [a.u.]: 94
        Temp. [C]: 24.90 -- RH [%]: 57.53 -- Gas [a.u.]: 30114 -- VOC [a.u.]: 94

