## CC1101

```
| length | address | payload |
|<- CC1101_MAX_PACKET_SIZE ->|
```

## TODOs
* [close-in reception](https://www.ti.com/lit/an/swra147b/swra147b.pdf)

receiver might be getting stuck when...
* being flooded with packets while waiting for a state transistion
* -> worker thread should administer state
* -> worker thread should be handling rxfifo_overflows
* -> worker thread should handle close-in reception
* -> worker thread should ...
