#ifndef APP_CC1101_H_
#define APP_CC1101_H_

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <usb/usb_device.h>
#include <drivers/uart.h>
#include <stdio.h>

#include <drivers/rf.h>
//#include <drivers/rf/cc1101.h>

#define THREAD_STACK_SIZE	1024
#define SLEEP_INTERVAL_RECV	100U
#define SLEEP_INTERVAL_SEND	500U

#define MAX_DATA_SIZE	CONFIG_CC1101_MAX_PACKET_SIZE
#define PAYLOAD_SIZE	MAX_DATA_SIZE-2 // - len and addr byte

struct glob {
	const struct device *rf;
	struct gpio_dt_spec led;
	bool sender_flag;
	struct k_mutex mutex;
};

extern struct glob global;

int gpio_setup(void);

void recv_thread(void *rf_device, void *p2, void *p3);
void send_thread(void *rf_device, void *p2, void *p3);

static inline int rssi_to_dbm(uint8_t rssi)
{
	int rssi_dbm;

	if (rssi >= 128){
		rssi_dbm = ((float)rssi - 256U) / 2;
	} else {
		rssi_dbm = (float)rssi / 2;
	}
	rssi_dbm = rssi_dbm - 74U;
	return rssi_dbm;
}

#endif // APP_CC1101_H_
