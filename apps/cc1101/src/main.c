/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"

struct glob global;

K_THREAD_STACK_DEFINE(recv_stack_area, THREAD_STACK_SIZE);
struct k_thread recv_thread_data;

K_THREAD_STACK_DEFINE(send_stack_area, THREAD_STACK_SIZE);
struct k_thread send_thread_data;

void event_handler(const struct device *rf_dev, enum rf_event event, void *event_params)
{
	switch(event)
	{
	case RF_EVENT_STUCK:
		printk("Stuck!\n");
		break;
	case RF_EVENT_RECV_READY:
		printk("Recv ready!\n");
		break;
	case RF_EVENT_SEND_READY:
		printk("Send ready!\n");
		break;
	case RF_EVENT_RECV_IN_PROGRESS:
		printk("Recv in progress!\n");
		break;
	case RF_EVENT_SEND_IN_PROGRESS:
		printk("Send in progress!\n");
		break;
	case RF_EVENT_RECV_DONE:
		printk("Recv done!\n");
		break;
	case RF_EVENT_SEND_DONE:
		printk("Send done!\n");
		break;
	case RF_EVENT_CHANNEL_CLEAR:
		printk("Channel Clear!\n");
		break;
	case RF_EVENT_WAKEUP:
		printk("WAKEUP!\n");
		break;
	default:
		printk("UNKNOWN EVENT\n");
	}
}

void main(void)
{
	const struct device *usb;
	const struct device *rf;

	k_mutex_init(&global.mutex);
	global.sender_flag = false;

	if (gpio_setup() != 0){
		printk("Failed setting up GPIO devices!\n");
		return;
	}

	usb = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);
	if (usb_enable(NULL)) {
		printk("Failed enabling USB device!\n");
		return;
	}

	while (!device_is_ready(usb)) {
		printk("Waiting for USB device...\n");
		k_sleep(K_MSEC(100));
	}

	rf = device_get_binding("CC1101");
	if (rf == NULL) {
		printk("Failed getting RF device!\n");
		return;
	}

	if(rf_device_set(rf, RF_DEVICE_SET_EVENT_HANDLER, event_handler)) {
		printk("Failed setting event handler for RF device!\n");
		return;
	}

	k_tid_t recv_tid = k_thread_create(&recv_thread_data, recv_stack_area,
					 K_THREAD_STACK_SIZEOF(recv_stack_area),
					 recv_thread,
					 (void *)rf, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);
	k_thread_name_set(recv_tid, "cc1101_sample_recv_thread");

	k_tid_t send_tid = k_thread_create(&send_thread_data, send_stack_area,
					 K_THREAD_STACK_SIZEOF(send_stack_area),
					 send_thread,
					 (void *)rf, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);
	k_thread_name_set(send_tid, "cc1101_sample_send_thread");

	k_thread_join(&recv_thread_data, K_FOREVER);
	k_thread_join(&send_thread_data, K_FOREVER);

	printk("Exiting main()...\n");
	return;
}
