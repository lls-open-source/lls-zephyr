/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <stdio.h>
#include <logging/log.h>

#include <drivers/counter.h>
#include <sys/timeutil.h>

#include <drivers/sensor/adp5360.h>
#include <drivers/rtc/maxim_max31343.h>


LOG_MODULE_REGISTER(APP, CONFIG_LOG_MAX_LEVEL);

#define TIMESTR_SIZE 24
#define DELAY 4000000
#define ALARM_CHANNEL_ID 0

struct counter_alarm_cfg alarm_cfg;

int get_timestr(const struct device * rtc, char * buf)
{
	uint32_t ticks;
	time_t time;
	int rc;
	struct tm time_buffer = { 0 };

	rc = counter_get_value(rtc, &ticks);
	if (rc) {
		LOG_ERR("Error getting counter value\n");
		return rc;
	}

	time = (time_t) ticks;
	gmtime_r(&time, &time_buffer);
	strftime(buf, TIMESTR_SIZE, "%Y-%m-%d %H:%M:%S %a", &time_buffer);

	return rc;
}

static void counter_interrupt_cb(const struct device *counter_dev,
				      uint8_t chan_id, uint32_t ticks,
				      void *user_data)
{
	struct counter_alarm_cfg *config = user_data;
	uint32_t now_ticks;
	int now_sec;
	int err;

	err = counter_get_value(counter_dev, &now_ticks);
	if (err) {
		printk("Failed to read counter value (err %d)", err);
		return;
	}

	//now_usec = counter_ticks_to_us(counter_dev, now_ticks);
	//now_sec = (int)(now_usec / USEC_PER_SEC);
	now_sec = now_ticks;

	printk("!!! Alarm !!!\n");
	printk("Now: %u\n", now_sec);

	/* Set a new alarm with a double length duration */
	config->ticks = config->ticks * 4U;

	printk("Set alarm in %u sec (%u ticks)\n",
	       (uint32_t)(counter_ticks_to_us(counter_dev,
					   config->ticks) / USEC_PER_SEC),
	       config->ticks);

	err = counter_set_channel_alarm(counter_dev, ALARM_CHANNEL_ID,
					user_data);
	if (err != 0) {
		printk("Alarm could not be set\n");
	}
}

int rtc_init(const struct device * rtc)
{
	int rc;
	time_t unix_time = 1631095620; /* Wed Sep 08 2021 10:07:00 */

	if (!device_is_ready(rtc)) {
		printk("Device %s is not ready.\n", rtc->name);
		return -ENODEV;
	}

	//counter_start(rtc);

	/* set the clock */
	rc = max31343_rtc_set_time(rtc, unix_time);
	if (rc) {
		printk("Failed setting time\n");
	}
	/* wait a bit */
	k_msleep(2000);

	alarm_cfg.flags = 0 | MAX31343_ALM1_Y_M_DT_T;
	alarm_cfg.ticks = counter_us_to_ticks(rtc, DELAY);
	alarm_cfg.callback = counter_interrupt_cb;
	alarm_cfg.user_data = &alarm_cfg;

	rc = counter_set_channel_alarm(rtc, ALARM_CHANNEL_ID,
					&alarm_cfg);

	printk("Set alarm in %u sec (%u ticks)\n",
	       (uint32_t)(counter_ticks_to_us(rtc,
					   alarm_cfg.ticks) / USEC_PER_SEC),
	       alarm_cfg.ticks);

	if (rc) {
		printk("Alarm settings invalid\n");
	}

	return rc;
}


int adp_init(const struct device * adp)
{
	int rc = 0;
	if (!device_is_ready(adp)) {
		printk("Device %s is not ready.\n", adp->name);
		return -ENODEV;
	}
	return rc;
}

void main(void)
{
	const struct device *adp = DEVICE_DT_GET_ANY(adi_adp5360);
	const struct device *rtc = DEVICE_DT_GET_ANY(maxim_max31343);
	int rc;
	char timestr_buf[TIMESTR_SIZE];


	rc = rtc_init(rtc);
	rc = adp_init(adp);

	for (;;) {
		rc = get_timestr(rtc, timestr_buf);
		if (rc) {
			LOG_ERR("Failed getting timestr\n");
			break;
		}
		printk("Time: %s\n", timestr_buf);

		k_msleep(2000);
	}

	printk("kthxbye.\n");
}
