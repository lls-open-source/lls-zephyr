/*
 * Copyright (c) 2018 Jan Van Winkel <jan.van_winkel@dxplore.eu>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <drivers/display.h>
#include <lvgl.h>
#include <stdio.h>
#include <string.h>
#include <zephyr.h>

#include <drivers/sensor.h>
#include <drivers/sensor/sgp40.h>
#include <drivers/sensor/sht4x.h>
#include <sensirion_voc_algorithm.h>

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(app);

#include <usb/usb_device.h>

void main(void)
{

	const struct device *dev;

	dev = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);
	if (dev == NULL || usb_enable(NULL)) {
		return;
	}

	/* Setup Sensors */
	const struct device *sht = DEVICE_DT_GET_ANY(sensirion_sht4x);
	const struct device *sgp = DEVICE_DT_GET_ANY(sensirion_sgp40);

	struct sensor_value comp_t;
	struct sensor_value comp_rh;
	struct sensor_value temp, hum, gas;
	static VocAlgorithmParams voc_algorithm_params;
	int32_t voc_index;
	VocAlgorithm_init(&voc_algorithm_params, 1); // 1s sampling interval

	if (!device_is_ready(sht)) {
		printf("Device %s is not ready.\n", sht->name);
		return;
	}

	if (!device_is_ready(sgp)) {
		printf("Device %s is not ready.\n", sgp->name);
		return;
	}

	/* Setup Display */
	const struct device *display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);

	lv_obj_t *sensor_label, *value_label;
	lv_obj_t *t_label, *h_label, *g_label, *v_label;
	lv_obj_t *t_val_label, *h_val_label, *g_val_label, *v_val_label;
	char num_buf[10];

	if (display_dev == NULL) {
		LOG_ERR("device not found.  Aborting test.");
		return;
	}

	sensor_label = lv_label_create(lv_scr_act(), NULL);
	value_label = lv_label_create(lv_scr_act(), NULL);
	t_label = lv_label_create(lv_scr_act(), NULL);
	h_label = lv_label_create(lv_scr_act(), NULL);
	g_label = lv_label_create(lv_scr_act(), NULL);
	v_label = lv_label_create(lv_scr_act(), NULL);
	t_val_label = lv_label_create(lv_scr_act(), NULL);
	h_val_label = lv_label_create(lv_scr_act(), NULL);
	g_val_label = lv_label_create(lv_scr_act(), NULL);
	v_val_label = lv_label_create(lv_scr_act(), NULL);

	lv_obj_align(sensor_label, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 30);
	lv_obj_align(value_label, NULL, LV_ALIGN_IN_TOP_MID, 20, 30);

	lv_obj_align(t_label, sensor_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 40);
	lv_obj_align(h_label, t_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);
	lv_obj_align(g_label, h_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);
	lv_obj_align(v_label, g_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);

	lv_obj_align(t_val_label, value_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 40);
	lv_obj_align(h_val_label, t_val_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);
	lv_obj_align(g_val_label, h_val_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);
	lv_obj_align(v_val_label, g_val_label, LV_ALIGN_OUT_BOTTOM_MID, 0, 20);

	lv_label_set_text_static(sensor_label, "Sensor" LV_SYMBOL_EYE_OPEN);
	lv_label_set_text_static(value_label, "Value" LV_SYMBOL_TRASH);
	lv_label_set_text_static(t_label, "Temp");
	lv_label_set_text_static(h_label, "Hum");
	lv_label_set_text_static(g_label, "Gas");
	lv_label_set_text_static(v_label, "VOC");

	lv_task_handler();
	display_blanking_off(display_dev);

	int counter = 0;
	k_sleep(K_MSEC(2000));
	while (1) {

		if (sensor_sample_fetch(sht)) {
			printf("Failed to fetch sample from SHT4X device\n");
			return;
		}

		sensor_channel_get(sht, SENSOR_CHAN_AMBIENT_TEMP, &temp);
		sensor_channel_get(sht, SENSOR_CHAN_HUMIDITY, &hum);

		comp_t.val1 = temp.val1;
		comp_rh.val1 = hum.val1;
		sensor_attr_set(sgp,
				SENSOR_CHAN_GAS_RES,
				SENSOR_ATTR_SGP40_TEMPERATURE,
				&comp_t);
		sensor_attr_set(sgp,
				SENSOR_CHAN_GAS_RES,
				SENSOR_ATTR_SGP40_HUMIDITY,
				&comp_rh);

		if (sensor_sample_fetch(sgp)) {
			printf("Failed to fetch sample from SGP40 device.\n");
			return;
		}

		sensor_channel_get(sgp, SENSOR_CHAN_GAS_RES, &gas);

		VocAlgorithm_process(&voc_algorithm_params, gas.val1, &voc_index);

		/*
		printf("Temp. [C]: %.2f -- RH [%%]: %0.2f -- Gas [a.u.]: %d -- VOC [a.u.]: %d\n",
		       sensor_value_to_double(&temp),
		       sensor_value_to_double(&hum),
		       gas.val1,
		       voc_index);
		*/
		sprintf(num_buf, "%.2f [C]", sensor_value_to_double(&temp));
		lv_label_set_text_fmt(t_val_label, "%s", num_buf);
		sprintf(num_buf, "%.2f [%%]", sensor_value_to_double(&hum));
		lv_label_set_text_fmt(h_val_label, "%s", num_buf);
		lv_label_set_text_fmt(g_val_label, "%d", gas.val1);
		lv_label_set_text_fmt(v_val_label, "%d", voc_index);

		/* Full refresh every 20 updates */
		counter++;
		if (!(counter % 20)){
			display_blanking_on(display_dev);
			display_blanking_off(display_dev);
		}

		lv_task_handler();
		k_sleep(K_MSEC(1000));
	}
}
