#include "app.h"

static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET(DT_NODELABEL(button0), gpios);
static struct gpio_callback button_cb_data;

static struct gpio_dt_spec led = GPIO_DT_SPEC_GET(DT_NODELABEL(led0), gpios);
static struct gpio_dt_spec pause = GPIO_DT_SPEC_GET(DT_NODELABEL(pause), gpios);


void button_pressed(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
	printk("Button pressed at %u -- toggling mode\n", k_cycle_get_32());

	gpio_pin_toggle_dt(&led);
	gpio_pin_toggle_dt(&global.indi);
}

int gpio_setup(void)
{
	int ret;

	if (!device_is_ready(button.port)) {
		printk("Error: button device %s is not ready\n",
		       button.port->name);
		return -ENODEV;
	}

	ret = gpio_pin_configure_dt(&button, (GPIO_INPUT | GPIO_INT_DEBOUNCE));
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n",
		       ret, button.port->name, button.pin);
		return ret;
	}

	ret = gpio_pin_interrupt_configure_dt(&button,
					      GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure interrupt on %s pin %d\n",
			ret, button.port->name, button.pin);
		return ret;
	}

	gpio_init_callback(&button_cb_data, button_pressed, BIT(button.pin));
	gpio_add_callback(button.port, &button_cb_data);
	printk("Set up button at %s pin %d\n", button.port->name, button.pin);

	if (!device_is_ready(led.port)) {
		printk("Error %d: LED device %s is not ready; ignoring it\n",
		       ret, led.port->name);
		led.port = NULL;
		return -ENODEV;
	}

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_INACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure LED device %s pin %d\n",
		       ret, led.port->name, led.pin);
		led.port = NULL;
		return ret;
	}

	printk("Set up LED at %s pin %d\n", led.port->name, led.pin);

	if (!device_is_ready(pause.port)) {
		printk("Error %d: LED device %s is not ready; ignoring it\n",
		       ret, pause.port->name);
		pause.port = NULL;
		return -ENODEV;
	}

	ret = gpio_pin_configure_dt(&pause, GPIO_OUTPUT_INACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure LED device %s pin %d\n",
		       ret, pause.port->name, pause.pin);
		pause.port = NULL;
		return ret;
	}

	printk("Set up LED at %s pin %d\n", pause.port->name, pause.pin);
	global.indi = pause;

	return ret;
}
