/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"

#define SIGNAL_PACKET_LENGTH	2U
#define SIGNAL_PACKET_ADDRESS	0x4C
#define SIGNAL_PACKET_COMMAND	0x1F

struct glob global;

const struct device *usb;
const struct device *rf;

union pkt_stat {
	uint8_t byte;
	struct {
		uint8_t lqi:7;
		uint8_t crc_ok:1;
	} __packed;
};

union packet_recv {
	uint8_t buf[SIGNAL_PACKET_LENGTH + 3]; // +1 length byte; +2 status bytes
	struct {
		uint8_t len;
		uint8_t addr;
		uint8_t cmd;
		uint8_t rssi;
		union pkt_stat stat;
	} __packed;
};

K_THREAD_STACK_DEFINE(recv_stack_area, THREAD_STACK_SIZE);
struct k_thread recv_thread_data;

K_THREAD_STACK_DEFINE(adc_stack_area, THREAD_STACK_SIZE);
struct k_thread adc_thread_data;

K_THREAD_STACK_DEFINE(play_stack_area, THREAD_STACK_SIZE);
struct k_thread play_thread_data;

void print_packet(union packet_recv *pkt)
{
	//printk("PPPPPP: %02X %02X %02X %02X %02X\n", pkt->buf[0], pkt->buf[1], pkt->buf[2], pkt->buf[3], pkt->buf[4]);
	printk("Received Signal: len=%d -- addr=%02X -- cmd=%02X -- rssi=%d -- crc=%d -- lqi=%d\n",
			pkt->len, pkt->addr, pkt->cmd, rssi_to_dbm(pkt->rssi), pkt->stat.crc_ok, pkt->stat.lqi);
}

int dev_init(void)
{
	usb = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);

	if (usb_enable(NULL)) {
		return -ENODEV;
	}

	while (!device_is_ready(usb)) {
		k_sleep(K_MSEC(100));
	}

	rf = device_get_binding("CC1101");
	if (rf == NULL) {
		return -ENODEV;
	}

	return 0;
}


void recv_thread(void *p1, void *p2, void *p3)
{
	int ret;
	union packet_recv pkt;

	ret = rf_set_mode(rf, RF_MODE_RX);

	while(true){
		/*
		if(rf_print_status(rf)) {
			printf("stat fail print!\n");
			return;
		}
		*/
		if(SIGNAL_PACKET_LENGTH == rf_recv(rf, (union rf_packet *)&pkt.buf)) {
			print_packet(&pkt);
			ret = rf_set_mode(rf, RF_MODE_RX);

			if(pkt.cmd == SIGNAL_PACKET_COMMAND && global.play_active) {
				k_mutex_lock(&global.recv_mutex, K_FOREVER);
				global.recv_flag = true;
				k_mutex_unlock(&global.recv_mutex);
			}
		}

		k_msleep(SLEEP_INTERVAL_MS);
	}
}

void main(void)
{
	if (gpio_setup() != 0){
		printk("Failed setting up GPIO devices!\n");
	}

	if (adc_setup() != 0){
		printk("Failed setting up ADC!\n");
	}

	if (play_setup() != 0){
		printk("Failed initializing play!\n");
	}

	if (dev_init() != 0){
		printk("Failed initializing devices!\n");
	}

	k_mutex_init(&global.recv_mutex);
	global.play_delay = 0;
	global.play_active = false;
	global.recv_flag = false;

	//k_tid_t recv_tid = k_thread_create(&recv_thread_data, recv_stack_area,
	k_thread_create(&recv_thread_data, recv_stack_area,
					 K_THREAD_STACK_SIZEOF(recv_stack_area),
					 recv_thread,
					 NULL, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	k_thread_create(&adc_thread_data, adc_stack_area,
					 K_THREAD_STACK_SIZEOF(adc_stack_area),
					 adc_thread,
					 NULL, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	k_thread_create(&play_thread_data, play_stack_area,
					 K_THREAD_STACK_SIZEOF(play_stack_area),
					 play_thread,
					 NULL, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	k_thread_join(&recv_thread_data, K_FOREVER);
	k_thread_join(&adc_thread_data, K_FOREVER);
	k_thread_join(&play_thread_data, K_FOREVER);

	printk("main() Exited!\n");
}
