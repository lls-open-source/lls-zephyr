#include "app.h"
#include <drivers/pwm.h>

/*
 * Devices
 */
#define SPEAKER_NODE	DT_ALIAS(speaker)

const struct device *speaker;

/*
 * PWM definitions
 */
#define PWM_CTLR	DT_PWMS_CTLR(SPEAKER_NODE)
#define PWM_CHANNEL	DT_PWMS_CHANNEL(SPEAKER_NODE)
#define PWM_FLAGS	DT_PWMS_FLAGS(SPEAKER_NODE)

#define MIN_PERIOD_USEC	(USEC_PER_SEC / 64U)
#define MAX_PERIOD_USEC	USEC_PER_SEC

/*
 * BZZZZZZZZZZZZZZZZZZ
 */
#define PWM_PERIOD_USEC	1500U
#define PWM_PULSE	450U
#define TONE_LENGTH	250U
#define TONE_REPEAT	8U
#define PWM_OFF_PULSE	0U

int ring_the_bell(void)
{
	int ret = 0;
	uint8_t onoff = 1;
	uint8_t repeat = 0;
	int pulse = PWM_PULSE;

	printk("DING GONG!\n");
	while (repeat < TONE_REPEAT){
		if (onoff) {
			pwm_pin_set_usec(speaker, PWM_CHANNEL, PWM_PERIOD_USEC, pulse, PWM_FLAGS);
			onoff = 0;
		} else {
			pwm_pin_set_usec(speaker, PWM_CHANNEL, PWM_PERIOD_USEC, PWM_OFF_PULSE, PWM_FLAGS);
			onoff = 1;
		}
		repeat += 1;
		k_msleep(TONE_LENGTH);
	}
	pwm_pin_set_usec(speaker, PWM_CHANNEL, PWM_PERIOD_USEC, 0, PWM_FLAGS);

	return ret;
}

void play_thread(void *p1, void *p2, void *p3)
{
	while(true){
		if (global.recv_flag && global.play_active){
			printk("play delay: %d seconds\n", global.play_delay);
			ring_the_bell();
			k_sleep(K_SECONDS(global.play_delay));
			k_mutex_lock(&global.recv_mutex, K_FOREVER);
			global.recv_flag = false;
			k_mutex_unlock(&global.recv_mutex);
		}
		k_msleep(SLEEP_INTERVAL_MS);
	}
}

int play_setup(void)
{
	int ret;
	ret = 0;

	speaker = DEVICE_DT_GET(PWM_CTLR);
	if (!device_is_ready(speaker)) {
		printk("Error: Speaker device %s is not ready\n", speaker->name);
		return -ENODEV;
	}

	printk("PWM MAX/MIN period: %d/%d \n", MAX_PERIOD_USEC, MIN_PERIOD_USEC);

	ret = pwm_pin_set_usec(speaker, PWM_CHANNEL, PWM_PERIOD_USEC, PWM_PULSE, PWM_FLAGS);
	if (ret != 0) {
		printk("PWM setup Error\n");
		return ret;
	}
	ret = pwm_pin_set_usec(speaker, PWM_CHANNEL, PWM_PERIOD_USEC, PWM_OFF_PULSE, PWM_FLAGS);
	if (ret != 0) {
		printk("PWM setup Error\n");
		return ret;
	}

	return ret;
}
