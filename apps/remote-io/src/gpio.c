#include "app.h"

#define REMOTE_IO_SPEC(node_id)					\
	{							\
		.gpio = GPIO_DT_SPEC_GET(node_id, gpios),	\
		.label = DT_PROP(node_id, channel_name),	\
		.cmd = DT_PROP(node_id, cmd),			\
	},


#if DT_NODE_EXISTS(DT_NODELABEL(remote_inputs))
struct remote_io inputs[] = {
	DT_FOREACH_CHILD(DT_NODELABEL(remote_inputs), REMOTE_IO_SPEC)
};
bool have_input = true;
#else
struct remote_io inputs[] = { 0 };
bool have_input = false;
#endif

#if DT_NODE_EXISTS(DT_NODELABEL(remote_outputs))
struct remote_io outputs[] = {
	DT_FOREACH_CHILD(DT_NODELABEL(remote_outputs), REMOTE_IO_SPEC)
};
bool have_output = true;
#else
struct remote_io outputs[] = { 0 };
bool have_output = false;
#endif

static struct gpio_callback gpio_cb_data;
void gpio_interrupt(const struct device *dev,
		struct gpio_callback *cb,
		uint32_t pins)
{
	int i;
	struct fifo_data_t fdata;

	if (k_uptime_delta(&global.timeout) > DEBOUNCE_MS) {

		for (i = 0; i < global.num_inputs; i++) {
			if(pins == BIT(global.inputs[i].gpio.pin)) {
				printk("Channel \"%s\" activated at %u\n", global.inputs[i].label, k_cycle_get_32());
				fdata.cmd = global.inputs[i].cmd;
				k_fifo_put(&global.input_fifo, &fdata);

				//gpio_pin_toggle_dt(&test_output.gpio);
			}
		}
		global.timeout = k_uptime_get();
	}
};

/*
struct remote_io test_input = {
	.gpio = GPIO_DT_SPEC_GET(DT_NODELABEL(tin), gpios),
	.label = DT_PROP(DT_NODELABEL(tin), channel_name),
	.cmd = DT_PROP(DT_NODELABEL(tin), cmd),
};

struct remote_io test_output = {
	.gpio = GPIO_DT_SPEC_GET(DT_NODELABEL(tout), gpios),
	.label = DT_PROP(DT_NODELABEL(tout), channel_name),
	.cmd = DT_PROP(DT_NODELABEL(tout), cmd),
};

static struct gpio_callback gpio_cb_data_test;
void gpio_interrupt_test(const struct device *dev,
		struct gpio_callback *cb,
		uint32_t pins)
{
	printk("Test GPIO activated at %u\n", k_cycle_get_32());
	gpio_pin_toggle_dt(&test_output.gpio);
};

int test_io_setup(void)
{
	int ret;

	if (!device_is_ready(test_input.gpio.port)) {
		printk("Error: input device %s is not ready\n",
		       test_input.gpio.port->name);
		return -ENODEV;
	}

	ret = gpio_pin_configure_dt(&test_input.gpio, (GPIO_INPUT | GPIO_INT_DEBOUNCE));
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n",
		       ret, test_input.gpio.port->name, test_input.gpio.pin);
		return ret;
	}

	ret = gpio_pin_interrupt_configure_dt(&test_input.gpio,
					      GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure interrupt on %s pin %d\n",
			ret, test_input.gpio.port->name, test_input.gpio.pin);
		return ret;
	}

	gpio_init_callback(&gpio_cb_data_test, gpio_interrupt_test, BIT(test_input.gpio.pin));
	gpio_add_callback(test_input.gpio.port, &gpio_cb_data_test);

	printk("Successfully set up test input channel \"%s\" (%s pin %d) (CMD: %d)\n",
			test_input.label,
			test_input.gpio.port->name,
			test_input.gpio.pin,
			test_input.cmd);

	if (!device_is_ready(test_output.gpio.port)) {
		printk("Error %d: output device %s is not ready\n",
		       ret, test_output.gpio.port->name);
		return -ENODEV;
	}

	ret = gpio_pin_configure_dt(&test_output.gpio, GPIO_OUTPUT_INACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure output device %s pin %d\n",
		       ret, test_output.gpio.port->name, test_output.gpio.pin);
		return ret;
	}

	printk("Successfully set up test output channel \"%s\" (%s pin %d) (CMD: %d)\n",
			test_output.label,
			test_output.gpio.port->name,
			test_output.gpio.pin,
			test_output.cmd);

	return ret;
}
*/

int gpio_setup(void)
{
	int i;
	int ret;
	int int_msk = 0;

	/*
	ret = test_io_setup();
	if (ret) {
		printk("Test Setup failed! Exiting...");
		return ret;
	}
	*/

	if (have_input) {
		global.inputs = inputs;
		global.num_inputs = sizeof(inputs) / sizeof(inputs[0]);

		printk("Initializing %d inputs:\n", global.num_inputs);

		for (i = 0; i < global.num_inputs; i++) {

			if (!device_is_ready(global.inputs[i].gpio.port)) {
				printk("Error: input device %s is not ready\n",
				       global.inputs[i].gpio.port->name);
				return -ENODEV;
			}

			ret = gpio_pin_configure_dt(&global.inputs[i].gpio, (GPIO_INPUT | GPIO_INT_DEBOUNCE));
			if (ret != 0) {
				printk("Error %d: failed to configure %s pin %d\n",
				       ret, global.inputs[i].gpio.port->name, global.inputs[i].gpio.pin);
				return ret;
			}

			ret = gpio_pin_interrupt_configure_dt(&global.inputs[i].gpio,
							      GPIO_INT_EDGE_TO_ACTIVE);
			if (ret != 0) {
				printk("Error %d: failed to configure interrupt on %s pin %d\n",
					ret, global.inputs[i].gpio.port->name, global.inputs[i].gpio.pin);
				return ret;
			}

			int_msk |= BIT(global.inputs[i].gpio.pin);

			printk("Successfully set up input channel \"%s\" (%s pin %d) (CMD: %d)\n",
					global.inputs[i].label,
					global.inputs[i].gpio.port->name,
					global.inputs[i].gpio.pin,
					global.inputs[i].cmd);
		}

		gpio_init_callback(&gpio_cb_data, gpio_interrupt, int_msk);
		gpio_add_callback(global.inputs[0].gpio.port, &gpio_cb_data);

	} else {
		printk("No inputs defined!\n");
	}

	if (have_output) {
		global.outputs = outputs;
		global.num_outputs = sizeof(outputs) / sizeof(outputs[0]);

		printk("Initializing %d outputs:\n", global.num_outputs);

		for (i = 0; i < global.num_outputs; i++) {

			if (!device_is_ready(global.outputs[i].gpio.port)) {
				printk("Error %d: output device %s is not ready\n",
				       ret, global.outputs[i].gpio.port->name);
				return -ENODEV;
			}

			ret = gpio_pin_configure_dt(&global.outputs[i].gpio, GPIO_OUTPUT_INACTIVE);
			if (ret != 0) {
				printk("Error %d: failed to configure output device %s pin %d\n",
				       ret, global.outputs[i].gpio.port->name, global.outputs[i].gpio.pin);
				return ret;
			}

			printk("Successfully set up output channel \"%s\" (%s pin %d) (CMD: %d)\n",
					global.outputs[i].label,
					global.outputs[i].gpio.port->name,
					global.outputs[i].gpio.pin,
					global.outputs[i].cmd);
		}
	} else {
		printk("No outputs defined!\n");
	}


	return ret;
}
