## bidirectional remote IO using CC1101

### Test thread
Uses the onboard GPIO pushbutton and LED

### App
The Inputs have to be on the same GPIO port for the app to work.
```
GPIO input <-> GPIO output
```
