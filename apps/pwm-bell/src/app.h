#ifndef APP_CC1101_H_
#define APP_CC1101_H_

#include <zephyr.h>
#include <device.h>
#include <stdio.h>

#define THREAD_STACK_SIZE	1024
#define SLEEP_INTERVAL_MS	100

int play_setup(void);
void play_thread(void *p1, void *p2, void *p3);

struct glob {
	uint8_t play_delay;
	bool play_active;

	// music
	int note_idx;
	uint8_t notes_repeat;
};

extern struct glob global;

#endif // APP_CC1101_H_
