/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"


struct glob global;

K_THREAD_STACK_DEFINE(play_stack_area, THREAD_STACK_SIZE);
struct k_thread play_thread_data;

void main(void)
{
	if (play_setup() != 0){
		printk("Failed initializing play!\n");
	}

	global.play_delay = 0;
	global.play_active = false;

	k_thread_create(&play_thread_data, play_stack_area,
					 K_THREAD_STACK_SIZEOF(play_stack_area),
					 play_thread,
					 NULL, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	k_thread_join(&play_thread_data, K_FOREVER);

	printk("main() Exited!\n");
}
