# Appz
## [LLS2 Sensor App](/apps/lls2-sensor-shell)
Zephyr's SHT40/SGP40 example app + Sensirion's VOC index algorithm.
## [LLS2 Sensor Shell](/apps/lls2-sensor-shell)
Zephyr's USB sensor shell application for the SGP40 and SHT40.
## [E-Ink Sensor App](/apps/epd-sensor)
SHT40/SGP40 + Sensirion's VOC Index driver using a GDEW027W3 e-ink display.
Look at this [YouTube video](https://www.youtube.com/watch?v=wuKjnCGz7X0) to see it in action.
## [E-Ink Display minimal example](/apps/epd)
Simple lvgl app for display driver development.
## [LLS3 minimal dev app](/apps/lls3)
WIP…
## [LLS-CC1101 minimal dev app](/apps/lls-cc1101)
WIP…

### Drivers
* display: il91874
* rtc: max31343
* pmic: adp5360
* rf: cc1101

### RF subsystem potential hardware
#### rf
* (nRF905)[https://infocenter.nordicsemi.com/index.jsp?topic=%2Fstruct_nrf9%2Fstruct%2Fnrf905.html]
* (Si446x)[https://www.silabs.com/wireless/proprietary/ezradiopro-sub-ghz-ics/device.si4463]
* (SX1212)[https://www.semtech.com/products/wireless-rf/frequency-shift-keying-fsk/sx1212]
#### nfc
* (TRF7964A)[https://www.ti.com/product/TRF7964A]
