# SPDX-License-Identifier: Apache-2.0

zephyr_library()
zephyr_library_sources_ifdef(CONFIG_IEEE802154_CC1101		ieee802154_cc1101.c)
