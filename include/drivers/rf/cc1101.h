/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef ZEPHYR_INCLUDE_DRIVERS_RF_CC1101_H_
#define ZEPHYR_INCLUDE_DRIVERS_RF_CC1101_H_

/* Receive Chain Attenuation for Close-in Reception */
#define CC1101_CLOSE_IN_RX_0DB  0x0 /* default */
#define CC1101_CLOSE_IN_RX_6DB  0x1
#define CC1101_CLOSE_IN_RX_12DB 0x2
#define CC1101_CLOSE_IN_RX_18DB 0x3

/* TX/RX FIFO Thresholds */
#define CC1101_FIFO_THR_61_04	0x0
#define CC1101_FIFO_THR_57_08	0x1
#define CC1101_FIFO_THR_53_12	0x2
#define CC1101_FIFO_THR_49_16	0x3
#define CC1101_FIFO_THR_45_20	0x3
#define CC1101_FIFO_THR_41_24	0x4
#define CC1101_FIFO_THR_37_28	0x5
#define CC1101_FIFO_THR_33_32	0x6 /* default */
#define CC1101_FIFO_THR_29_36	0x7
#define CC1101_FIFO_THR_25_40	0x8
#define CC1101_FIFO_THR_21_44	0x9
#define CC1101_FIFO_THR_17_48	0xa
#define CC1101_FIFO_THR_13_52	0xb
#define CC1101_FIFO_THR_09_56	0xc
#define CC1101_FIFO_THR_05_60	0xd
#define CC1101_FIFO_THR_01_64	0xe

/* Address Check Configuration */
#define CC1101_ADDR_CHK_NONE	0x0 /* default */
#define CC1101_ADDR_CHK_NO_BRD	0x1
#define CC1101_ADDR_CHK_BRD	0x2
#define CC1101_ADDR_CHK_ALL_BRD	0x3

/* Packet Format */
#define CC1101_PKT_FORMAT_NORMAL	0x0 /* default */
#define CC1101_PKT_FORMAT_SYNC_SERIAL	0x1
#define CC1101_PKT_FORMAT_RANDOM_TEST	0x2
#define CC1101_PKT_FORMAT_ASYNC_SERIAL	0x3

/* Packet Length Configuration */
#define CC1101_PKT_LEN_FIXED	0x0 /* default */
#define CC1101_PKT_LEN_VARIABLE	0x1
#define CC1101_PKT_LEN_INFINITE	0x2

/* Modulation Format */
#define CC1101_MOD_FORMAT_2FSK	0x0 /* default */
#define CC1101_MOD_FORMAT_GFSK	0x1
#define CC1101_MOD_FORMAT_ASK	0x3
#define CC1101_MOD_FORMAT_4FSK	0x4
#define CC1101_MOD_FORMAT_MSK	0x7

/* Sync Mode */
#define CC1101_SYNC_MODE_NONE		0x0
#define CC1101_SYNC_MODE_15_16		0x1
#define CC1101_SYNC_MODE_16_16		0x2 /* default */
#define CC1101_SYNC_MODE_30_32		0x3
#define CC1101_SYNC_MODE_CS_ONLY	0x4
#define CC1101_SYNC_MODE_CS_15_16	0x5
#define CC1101_SYNC_MODE_CS_16_16	0x6
#define CC1101_SYNC_MODE_CS_30_32	0x7

/* Preamble Bytes */
#define CC1101_NUM_PREAMBLE_2	0x0
#define CC1101_NUM_PREAMBLE_3	0x1
#define CC1101_NUM_PREAMBLE_4	0x2 /* default */
#define CC1101_NUM_PREAMBLE_6	0x3
#define CC1101_NUM_PREAMBLE_8	0x4
#define CC1101_NUM_PREAMBLE_12	0x5
#define CC1101_NUM_PREAMBLE_16	0x6
#define CC1101_NUM_PREAMBLE_24	0x7

/* Clear Channel Assessment (CCA) Mode */
#define CC1101_CCA_ALWAYS		0x0
#define CC1101_CCA_RSSI_BELOW_THR	0x1
#define CC1101_CCA_UNLESS_RECV		0x2
#define CC1101_CCA_BOTH			0x3 /* default */

/* RX Off Mode */
#define CC1101_RX_OFF_IDLE	0x0 /* default */
#define CC1101_RX_OFF_FSTXON	0x1
#define CC1101_RX_OFF_TX	0x2
#define CC1101_RX_OFF_STAY_RX	0x3

/* TX Off Mode */
#define CC1101_TX_OFF_IDLE	0x0 /* default */
#define CC1101_TX_OFF_FSTXON	0x1
#define CC1101_TX_OFF_STAY_TX	0x2
#define CC1101_TX_OFF_RX	0x3

/* Autocalibration */
#define CC1101_AUTOCAL_NEVER		0x0 /* default */
#define CC1101_AUTOCAL_IDLE_TO_RXTX	0x1
#define CC1101_AUTOCAL_RXTX_TO_IDLE	0x2
#define CC1101_AUTOCAL_EVERY_4TH	0x3

/* PO Timeout */
#define CC1101_PO_TIMEOUT_1	0x0 /* default */
#define CC1101_PO_TIMEOUT_16	0x1
#define CC1101_PO_TIMEOUT_64	0x2
#define CC1101_PO_TIMEOUT_256	0x3

/* Frequency Offset Compensation Loop Gain Before Sync Word Detection */
#define CC1101_FOC_PRE_K	0x0
#define CC1101_FOC_PRE_2K	0x1
#define CC1101_FOC_PRE_3K	0x2 /* default */
#define CC1101_FOC_PRE_4K	0x3

/* Frequency Offset Compensation Loop Gain After Sync Word Detection */
#define CC1101_FOC_POST_SAME_AS_PRE	0x0
#define CC1101_FOC_POST_HALF_OF_PRE	0x1 /* default */

/* Frequency Offset Compensation Algorithm */
#define CC1101_FOC_LIMIT_NONE		0x0
#define CC1101_FOC_LIMIT_BW_DIV_8	0x1
#define CC1101_FOC_LIMIT_BW_DIV_4	0x2 /* default */
#define CC1101_FOC_LIMIT_BW_DIV_2	0x3

/* Bit Synchronization Pre Sync Word Detection */
#define CC1101_PRE_KI_KI	0x0
#define CC1101_PRE_KI_2KI	0x1 /* default */
#define CC1101_PRE_KI_3KI	0x2
#define CC1101_PRE_KI_4KI	0x3

/* Clock  */
#define CC1101_PRE_KP_KP	0x0
#define CC1101_PRE_KP_2KP	0x1
#define CC1101_PRE_KP_3KP	0x2 /* default */
#define CC1101_PRE_KP_4KP	0x3

/* Clock  */
#define CC1101_POST_KI_SAME_AS_PRE	0x0
#define CC1101_POST_KI_HALF_OF_PRE	0x1 /* default */

/* Clock  */
#define CC1101_POST_KP_SAME_AS_PRE	0x0
#define CC1101_POST_KP_HALF_OF_PRE	0x1 /* default */

/* Clock  */
#define CC1101_BS_LIMIT_0	0x0 /* default */
#define CC1101_BS_LIMIT_3	0x1
#define CC1101_BS_LIMIT_6	0x2
#define CC1101_BS_LIMIT_12	0x3

/* Clock  */
#define CC1101_MAX_DVA_GAIN_ALL			0x0 /* default */
#define CC1101_MAX_DVA_GAIN_NOT_HIGHEST		0x1
#define CC1101_MAX_DVA_GAIN_NOT_HIGHEST_2	0x2
#define CC1101_MAX_DVA_GAIN_NOT_HIGHEST_3	0x3

/* Clock  */
#define CC1101_MAX_LNA_GAIN_MAX		0x0 /* default */
#define CC1101_MAX_LNA_GAIN_2_BELOW	0x1
#define CC1101_MAX_LNA_GAIN_6_BELOW	0x2
#define CC1101_MAX_LNA_GAIN_7_BELOW	0x3
#define CC1101_MAX_LNA_GAIN_9_BELOW	0x4
#define CC1101_MAX_LNA_GAIN_11_BELOW	0x5
#define CC1101_MAX_LNA_GAIN_14_BELOW	0x6
#define CC1101_MAX_LNA_GAIN_17_BELOW	0x7

/* Clock  */
#define CC1101_MAGN_TARGET_24	0x0
#define CC1101_MAGN_TARGET_27	0x1
#define CC1101_MAGN_TARGET_30	0x2
#define CC1101_MAGN_TARGET_33	0x3 /* default */
#define CC1101_MAGN_TARGET_36	0x4
#define CC1101_MAGN_TARGET_38	0x5
#define CC1101_MAGN_TARGET_40	0x6
#define CC1101_MAGN_TARGET_42	0x7

/* Clock  */
#define CC1101_CS_REL_THR_DISABLE	0x0 /* default */
#define CC1101_CS_REL_THR_PLUS_6	0x1
#define CC1101_CS_REL_THR_PLUS_10	0x2
#define CC1101_CS_REL_THR_PLUS_14	0x3

/* Clock  */
#define CC1101_CS_ABS_THR_DISABLE	0x0 /* default */
#define CC1101_CS_ABS_THR_MINUS_7	0x1
#define CC1101_CS_ABS_THR_MINUS_6	0x2
#define CC1101_CS_ABS_THR_MINUS_5	0x3
#define CC1101_CS_ABS_THR_MINUS_4	0x4
#define CC1101_CS_ABS_THR_MINUS_3	0x5
#define CC1101_CS_ABS_THR_MINUS_2	0x6
#define CC1101_CS_ABS_THR_MINUS_1	0x7
#define CC1101_CS_ABS_THR_MAGN_TARGET	0x8
#define CC1101_CS_ABS_THR_PLUS_1	0x9
#define CC1101_CS_ABS_THR_PLUS_2	0xa
#define CC1101_CS_ABS_THR_PLUS_3	0xb
#define CC1101_CS_ABS_THR_PLUS_4	0xc
#define CC1101_CS_ABS_THR_PLUS_5	0xd
#define CC1101_CS_ABS_THR_PLUS_6	0xe
#define CC1101_CS_ABS_THR_PLUS_7	0xf

/* Clock  */
#define CC1101_HYST_LEVEL_NONE	0x0
#define CC1101_HYST_LEVEL_LOW	0x1
#define CC1101_HYST_LEVEL_MED	0x2 /* default */
#define CC1101_HYST_LEVEL_HIGH	0x3

/* Clock  */
#define CC1101_WAIT_TIME_8	0x0
#define CC1101_WAIT_TIME_16	0x1 /* default */
#define CC1101_WAIT_TIME_24	0x2
#define CC1101_WAIT_TIME_32	0x3

/* Clock  */
#define CC1101_AGC_FREEZE_ALWAYS	0x0 /* default */
#define CC1101_AGC_FREEZE_AFTER_SYNC	0x1
#define CC1101_AGC_FREEZE_MANUAL_ANALOG	0x2
#define CC1101_AGC_FREEZE_MANUAL_ALL	0x3

/* Clock  */
#define CC1101_FILTER_LENGTH_8_4	0x0
#define CC1101_FILTER_LENGTH_16_8	0x1 /* default */
#define CC1101_FILTER_LENGTH_32_12	0x2
#define CC1101_FILTER_LENGTH_64_16	0x3

/* Clock  */
#define CC1101_EVENT1_4		0x0
#define CC1101_EVENT1_6		0x1
#define CC1101_EVENT1_8		0x2
#define CC1101_EVENT1_12	0x3
#define CC1101_EVENT1_16	0x4
#define CC1101_EVENT1_24	0x5
#define CC1101_EVENT1_32	0x6
#define CC1101_EVENT1_48	0x7 /* default */

/* Clock  */
#define CC1101_WOR_RES_2_SEC	0x0 /* default */
#define CC1101_WOR_RES_1_MIN	0x1
#define CC1101_WOR_RES_30_MIN	0x2
#define CC1101_WOR_RES_17_HR	0x3

/* Status Registers */
enum cc1101_status_reg {
	SLEEP = 0x00	, // SLEEP
	IDLE		, // IDLE
	XOFF		, // XOFF
	VCOON_MC	, // MANCAL
	REGON_MC	, // MANCAL
	MANCAL		, // MANCAL
	VCOON		, // FS_WAKEUP
	REGON		, // FS_WAKEUP
	STARTCAL	, // CALIBRATE
	BWBOOST		, // SETTLING
	FS_LOCK		, // SETTLING
	IFADCON		, // SETTLING
	ENDCAL		, // CALIBRATE
	RX		, // RX
	RX_END		, // RX
	RX_RST		, // RX
	TXRX_SWITCH	, // TXRX_SETTLING
	RXFIFO_OVERFLOW	, // RXFIFO_OVERFLOW
	FSTXON		, // FSTXON
	TX		, // TX
	TX_END		, // TX
	RXTX_SWITCH	, // RXTX_SETTLING
	TXFIFO_UNDERFLOW, // TXFIFO_UNDERFLOW
};

#endif // ZEPHYR_INCLUDE_DRIVERS_RF_CC1101_H_
