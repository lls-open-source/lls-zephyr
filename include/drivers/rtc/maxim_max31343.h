/*
 * Copyright (c) 2021 Laird Connectivity
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef ZEPHYR_INCLUDE_DRIVERS_RTC_MAX31343_H_
#define ZEPHYR_INCLUDE_DRIVERS_RTC_MAX31343_H_

#include <sys/timeutil.h>
#include <time.h>

#define MAX31343_ALM1_SECONDLY	BIT(0)
#define MAX31343_ALM1_S		BIT(1)
#define MAX31343_ALM1_M_S	BIT(2)
#define MAX31343_ALM1_H_M_S	BIT(3)
#define MAX31343_ALM1_DT_T	BIT(4)
#define MAX31343_ALM1_M_DT_T	BIT(5)
#define MAX31343_ALM1_Y_M_DT_T	BIT(6)
#define MAX31343_ALM1_DY_T	BIT(7)
#define MAX31343_ALM2_MINUTELY	BIT(8)
#define MAX31343_ALM2_M		BIT(9)
#define MAX31343_ALM2_H_M	BIT(10)
#define MAX31343_ALM2_DT_H_M	BIT(11)
#define MAX31343_ALM2_DY_H_M	BIT(12)

struct max31343_status {
	uint8_t a1f : 1;
	uint8_t a2f : 1;
	uint8_t tif : 1;
	uint8_t tsf : 1;
	uint8_t nimp : 1;
	uint8_t pfail : 1;
	uint8_t osf : 1;
	uint8_t psdect : 1;
} __packed;

struct max31343_int_en {
	uint8_t a1e : 1;
	uint8_t a2e : 1;
	uint8_t tie : 1;
	uint8_t tsie : 1;
	uint8_t nimp : 1;
	uint8_t pfaile : 1;
	uint8_t dosf : 1;
	uint8_t nimp2 : 1;
} __packed;

struct max31343_reset {
	uint8_t swrst : 1;
	uint8_t nimp : 7;
} __packed;

struct max31343_config1 {
	uint8_t reserved : 1;
	uint8_t enosc : 1;
	uint8_t nimp : 1;
	uint8_t i2c_timeout : 1;
	uint8_t data_ret : 1;
	uint8_t nimp2 : 3;
} __packed;

struct max31343_config2 {
	uint8_t sqw_hz : 3;
	uint8_t clko_hz : 4;
	uint8_t enclko : 1;
} __packed;

struct max31343_timer_config {
	uint8_t tfs : 2;
	uint8_t trpt : 1;
	uint8_t tpause : 1;
	uint8_t te : 1;
	uint8_t nimp : 3;
} __packed;

struct max31343_rtc_sec {
	uint8_t sec_one : 4;
	uint8_t sec_ten : 3;
	uint8_t nimp : 1;
} __packed;

struct max31343_rtc_min {
	uint8_t min_one : 4;
	uint8_t min_ten : 3;
	uint8_t nimp : 1;
} __packed;

struct max31343_rtc_hours {
	uint8_t hr_one : 4;
	uint8_t hr_ten : 1;
	uint8_t nimp : 2;
} __packed;

struct max31343_rtc_weekday {
	uint8_t weekday : 3;
	uint8_t nimp : 5;
} __packed;

struct max31343_rtc_date {
	uint8_t date_one : 4;
	uint8_t date_ten : 2;
	uint8_t nimp : 2;
} __packed;

struct max31343_rtc_month {
	uint8_t month_one : 4;
	uint8_t month_ten : 1;
	uint8_t nimp : 2;
	uint8_t century : 1;
} __packed;

struct max31343_rtc_year {
	uint8_t year_one : 4;
	uint8_t year_ten : 4;
} __packed;

struct max31343_alm1_sec {
	uint8_t sec_one : 4;
	uint8_t sec_ten : 3;
	uint8_t a1m1 : 1;
} __packed;

struct max31343_alm1_min {
	uint8_t min_one : 4;
	uint8_t min_ten : 3;
	uint8_t a1m2 : 1;
} __packed;

struct max31343_alm1_hours {
	uint8_t hr_one : 4;
	uint8_t hr_ten : 1;
	uint8_t nimp : 2;
	uint8_t a1m3 : 1;
} __packed;

struct max31343_alm1_day_date {
	uint8_t day_date : 4;
	uint8_t date_ten : 2;
	uint8_t dy_dt : 1;
	uint8_t a1m4 : 1;
} __packed;

struct max31343_alm1_month {
	uint8_t month_one : 4;
	uint8_t month_ten : 1;
	uint8_t nimp : 1;
	uint8_t a1m6 : 1;
	uint8_t a1m5 : 1;
} __packed;

struct max31343_alm1_year {
	uint8_t year_one : 4;
	uint8_t year_ten : 4;
} __packed;

struct max31343_alm2_min {
	uint8_t min_one : 4;
	uint8_t min_ten : 3;
	uint8_t a2m2 : 1;
} __packed;

struct max31343_alm2_hours {
	uint8_t hr_one : 4;
	uint8_t hr_ten : 1;
	uint8_t nimp : 2;
	uint8_t a2m3 : 1;
} __packed;

struct max31343_alm2_day_date {
	uint8_t day_date : 4;
	uint8_t date_ten : 2;
	uint8_t dy_dt : 1;
	uint8_t a2m4 : 1;
} __packed;

struct max31343_pwr_mgmt {
	uint8_t nimp : 2;
	uint8_t d_man_sel : 1;
	uint8_t d_vbac_sel : 1;
	uint8_t pfvt : 2;
	uint8_t nimp2 : 2;
} __packed;

struct max31343_trickle {
	uint8_t d_trickle : 4;
	uint8_t tche : 4;
} __packed;

struct max31343_ts_config {
	uint8_t nimp : 3;
	uint8_t ttsint : 3;
	uint8_t oneshot_mode : 1;
	uint8_t auto_mode : 1;
} __packed;

struct max31343_config_registers {
	struct max31343_int_en int_en;
	struct max31343_reset reset;
	struct max31343_config1 config1;
	struct max31343_config2 config2;
	struct max31343_timer_config tmr_config;
	struct max31343_pwr_mgmt pwr_mgmt;
	struct max31343_trickle trickle;
	struct max31343_ts_config ts_config;
} __packed;

struct max31343_time_registers {
	struct max31343_rtc_sec rtc_sec;
	struct max31343_rtc_min rtc_min;
	struct max31343_rtc_hours rtc_hours;
	struct max31343_rtc_weekday rtc_weekday;
	struct max31343_rtc_date rtc_date;
	struct max31343_rtc_month rtc_month;
	struct max31343_rtc_year rtc_year;
} __packed;

struct max31343_alarm1_registers {
	struct max31343_alm1_sec alm_sec;
	struct max31343_alm1_min alm_min;
	struct max31343_alm1_hours alm_hours;
	struct max31343_alm1_day_date alm_day_date;
	struct max31343_alm1_month alm_month;
	struct max31343_alm1_year alm_year;
} __packed;

struct max31343_alarm2_registers {
	struct max31343_alm2_min alm_min;
	struct max31343_alm2_hours alm_hours;
	struct max31343_alm2_day_date alm_day_date;
} __packed;

enum max31343_register {
	REG_STATUS		= 0x0,
	REG_INT_EN		= 0x1,
	REG_RESET		= 0x2,
	REG_CONFIG1		= 0x3,
	REG_CONFIG2		= 0x4,
	REG_TMR_CONFIG		= 0x5,
	REG_RTC_SEC		= 0x6,
	REG_RTC_MIN		= 0x7,
	REG_RTC_HOUR		= 0x8,
	REG_RTC_WDAY		= 0x9,
	REG_RTC_DATE		= 0xA,
	REG_RTC_MONTH		= 0xB,
	REG_RTC_YEAR		= 0xC,
	REG_ALM1_SEC		= 0xD,
	REG_ALM1_MIN		= 0xE,
	REG_ALM1_HOUR		= 0xF,
	REG_ALM1_DAY_DATE	= 0x10,
	REG_ALM1_MONTH		= 0x11,
	REG_ALM1_YEAR		= 0x12,
	REG_ALM2_MIN		= 0x13,
	REG_ALM2_HOUR		= 0x14,
	REG_ALM2_DAY_DATE	= 0x15,
	REG_TIMER_COUNT		= 0x16,
	REG_TIMER_INIT		= 0x17,
	REG_PWR_MGMT		= 0x18,
	REG_TRICKLE		= 0x19,
	REG_TEMP_MSB		= 0x1A,
	REG_TEMP_LSB		= 0x1B,
	REG_TS_CONFIG		= 0x1C,
	REG_SRAM_MIN		= 0x22,
	REG_SRAM_MAX		= 0x61,
};

/** @brief Set the RTC to a given Unix time
 *
 * The RTC advances one tick per second with no access to sub-second
 * precision. This function will convert the given unix_time into seconds,
 * minutes, hours, day of the week, day of the month, month and year.
 * A Unix time of '0' means a timestamp of 00:00:00 UTC on Thursday 1st January
 * 1970.
 *
 * @param dev the MAX31343 device pointer.
 * @param unix_time Unix time to set the rtc to.
 *
 * @retval return 0 on success, or a negative error code from an I2C
 * transaction or invalid parameter.
 */
int max31343_rtc_set_time(const struct device *dev, time_t unix_time);

#endif /* ZEPHYR_INCLUDE_DRIVERS_RTC_MAX31343_H_ */
