## Introduction

This is a proposal for a generic API for the pysical layer of RF frontends.

It is a rough sketch of the idea and I am not sure if this approach is even feasible.

### Problem description

Currently Zephyr does not have a device driver abstraction for interacting with an RF modems' pyhsical layer.

One examples of an [IEEE 802.15.4](https://github.com/zephyrproject-rtos/zephyr/tree/main/drivers/ieee802154) driver where this leads to problems is
the CC1200 which supports frequencies and modulation techniques for applications not covered by the IEEE 802.15.4 standard.

### Proposed change

Introduce an API for device drivers targeting the pyhsical layer of RF frontends.

This could provide a straight-forward path for e.g.

* [IEEE 802.15.4 PHYs](https://github.com/zephyrproject-rtos/zephyr/tree/main/drivers/ieee802154)
* [LoRa PHYs](https://github.com/zephyrproject-rtos/zephyr/tree/main/drivers/lora)
* [NFC/RFID](https://github.com/zephyrproject-rtos/zephyr/issues/5703)
* RF protocols outside of IEEE 802.15.4 and LoRa-MAC (wM-Bus, NB-Fi, propietary, ...)
* Real-time locating systems (RTLS)
* Custom analog RF frontends

without tying the driver into any protocol/subsystem layers.


## Detailed (draft) RFC

```C
/**
 * NOTE: different way to handle packets? e.g. network buffers?
 */
union rf_packet {
	uint8_t buffer[CONFIG_RF_MAX_PACKET_SIZE];
	struct {
		uint8_t length;
		uint8_t payload[CONFIG_RF_MAX_PACKET_SIZE - 1];
	} __packed;
};

typedef uint64_t rf_freq_t;

typedef uint16_t rf_chan_t;

typedef uint32_t rf_baud_t;

typedef int16_t rf_power_t;

enum rf_mod_format {
	RF_MODULATION_OOK = 0,
	RF_MODULATION_ASK,
	RF_MODULATION_2FSK,
	RF_MODULATION_4FSK,
	RF_MODULATION_GFSK,
	RF_MODULATION_AFSK,
	RF_MODULATION_2PSK,
	RF_MODULATION_MSK,
	RF_MODULATION_GMSK,
	RF_MODULATION_DSSS,
};

enum rf_op_mode {
	RF_MODE_CALIBRATE = 0,
	RF_MODE_POWER_OFF,
	RF_MODE_SLEEP,
	RF_MODE_IDLE,
	RF_MODE_RX_WAKE_ON_EVENT,
	RF_MODE_RX_WAKE_PERIODIC,
	RF_MODE_RX,
	RF_MODE_TX,
};

/**
 * -> see: rf_event_cb_t
 * -> using k_event object might be more sensible
 * -> define event bits
 */
enum rf_event {
	RF_EVENT_STUCK = 0,
	RF_EVENT_RECV_READY,
	RF_EVENT_SEND_READY,
	RF_EVENT_RECV_DONE,
	RF_EVENT_SEND_DONE,
	RF_EVENT_CHANNEL_CLEAR,
	RF_EVENT_WAKEUP,
};

enum rf_hw_filter {
	RF_HW_FILTER_NONE = 0,
	RF_HW_FILTER_CRC8,
	RF_HW_FILTER_CRC16,
	RF_HW_FILTER_ADDR,
	RF_HW_FILTER_LENGTH,
};

/**
 * making every entry in this enum a seperate API  function
 * will avoid the use of void pointers
 */
enum rf_device_arg {
	RF_DEVICE_FREQUENCY = 0,
	RF_DEVICE_CHANNEL,
	RF_DEVICE_MODULATION_FORMAT,
	RF_DEVICE_BAUDRATE,
	RF_DEVICE_OUTPUT_POWER,
	RF_DEVICE_OPERATING_MODE,
	RF_DEVICE_SET_EVENT_CB,
	RF_DEVICE_HW_FILTER,
	RF_DEVICE_SETTINGS,
	RF_DEVICE_CALIBRATION_SETTINGS,
	RF_DEVICE_POWER_TABLE,
};

/**
 * -> this will block the phy driver
 * -> using k_event object might be more sensible
 */
typedef void (*rf_event_cb_t)(const struct device *dev,
				enum rf_event event,
				void *event_params);

/**
 * see `enum rf_device_arg`
 */
typedef int (*rf_device_set_t)(const struct device *dev,
				    enum rf_device_arg cfg,
				    void *val);

typedef int (*rf_device_get_t)(const struct device *dev,
				    enum rf_device_arg cfg,
				    void *val);

typedef int (*rf_device_send_t)(const struct device *dev,
				     union rf_packet *pkt);

typedef int (*rf_device_recv_t)(const struct device *dev,
				     union rf_packet *pkt);

__subsystem struct rf_driver_api {
	rf_device_set_t device_set;
	rf_device_get_t device_get;
	rf_device_send_t send;
	rf_device_recv_t recv;
};
```

### Dependencies

Some already existing IEEE 802.15.4 and LoRa-MAC drivers MAY be adapted to use this API.

### Concerns and Unresolved Questions

There are a lot of open questions. To list a few:

* Instead of having `rf_device_set/rf_device_get` that accepts `enum rf_device_arg` and a void
pointer. Make a function for everything listed in the enum.

* Support for multiple channels (e.g. MIMO)?

* Use `net_buf` instead of `struct rf_packet`?!

* Usage of `k_event` instead of callbacks. This way one would end up with two rx threads
(one for the rf device and one in the upper layers) a dependency on `CONFIG_EVENTS`.

* Is there another/clean way to get the rf device from other drivers (see: )?

* There are devices in this category have a "direct" mode (might be a usecase for RTIO API proposed in https://github.com/zephyrproject-rtos/zephyr/pull/17434).

## Alternatives

The alternative is to deal with some code duplication for the PHY layer of multi-protocol rf frontends.

`CONFIG_IEEE802154_RAW_MODE`/`CONFIG_NET_RAW_MODE` are available and it might be sufficient to resort to these?
